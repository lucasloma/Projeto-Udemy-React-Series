import React from 'react';
import {StackNavigator,View, TextInput, StyleSheet, ActivityIndicator,Text,Button} from 'react-native';
import { FormRow} from '../components';
import firebase from 'firebase';

import { tryLogin} from '../actions'
import { connect } from 'react-redux'

class LoginPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            isLoading: false,
            message: ""
        }
    };

    /*
* Função para iniciar o FireBase
* @params: field nome da variável setada no formulário
* @since: 28/07/2018
* @version 1.0
* @author: Lucas Lobato Maciel
* */
    componentDidMount()
    {
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyDySIWvSdT43EDUOnGPKxt_R9F-KMeJZ_s",
            authDomain: "series-5b98d.firebaseapp.com",
            databaseURL: "https://series-5b98d.firebaseio.com",
            projectId: "series-5b98d",
            storageBucket: "series-5b98d.appspot.com",
            messagingSenderId: "1080103326407"
        };
        firebase.initializeApp(config);

    }

    render() {

        return (
            <View style={ styles.conteiner}>
                <FormRow first>
                    <TextInput
                        style={styles.formInput}
                        placeholder="Enter email"
                        value={this.state.email}
                        onChangeText={value => this.onChangeHandler('email',value)}
                    />
                </FormRow>
                <FormRow last>
                    <TextInput
                        style={styles.formInput}
                        placeholder="Enter senha"
                        secureTextEntry
                        value={this.state.password}
                        onChangeText={value => this.onChangeHandler('password',value)}
                    />
                </FormRow>
                {this.renderButtonLogin()}
                {this.renderMessage()}
            </View>

        );
    };


        /*
    * Função para pegar o valor digitado e setar novamente as varieveis defindas no this.state.
    * @params: field nome da variável setada no formulário
    * @params: value valor atribuido a variável
    * @since: 25/07/2018
    * @version 1.0
    * @author: Lucas Lobato Maciel
    * */


    onChangeHandler(field,value){

        this.setState({
            [field]:value /*seria como pegar o valor do campo e setar o mesmo*/
        });
    };


        /*
    * Função para pegar o valor do formulário de login e validar usuário e senha.
    * @params: field nome da variável setada no formulário
    * @params: value valor atribuido a variável
    * @since: 25/07/2018
    * @version 1.0
    * @author: Lucas Lobato Maciel
    * */

    tryLogin()
    {
        this.setState({isLoading:true , message:''});
        const {email,password} = this.state;
        this.props.tryLogin({email,password}).then((user)=>{
                if (user) {// if escrito de outra forma mas curta.

                    return this.props.navigation.replace('main',{user}); /// replace não deixa voltar limpa o histórico.
                }else {
                    this.setState({isLoading: false, message: 'Que pena vai deixar passar!!!!'});
                }
            }
        ).catch(error => {
            // console.log(error.code); para mostrar qual erro está acontecendo.
                this.setState({isLoading:false , message:this.getMessageError(error.code)});
            }

        );
    };

        /*
    * Função para retorna o tipo de erro para usuário
    * @param: sting erroCode
    * @since: 31/07/2018
    * @version 1.0
    * @author: Lucas Lobato Maciel
    * */

    getMessageError(erroCode)
    {
        switch(erroCode){
            case 'auth/invalid-email':
                return 'Email Inválido';
            case 'auth/wrong-password':
                return 'Senha Inválida';
            case 'auth/user-not-found':
                return 'Usuário Inválido';
            case 'auth/weak-password':
                return 'Sua Senha Precisa ter no minímo 6 caracteres';
            default:
                return 'Erro Desconhecido';
        }

    };

        /*
    * Função renderizar o botao de login
    * @since: 28/07/2018
    * @version 1.0
    * @author: Lucas Lobato Maciel
    * */

    renderButtonLogin(){

        if(this.state.isLoading){
           return <ActivityIndicator/>
        }

        return(

            <Button
                large
                color={'blue'}
                title='Entrar'
                onPress={()=> this.tryLogin()}/>
        );
    };

        /*
    * Função renderizar messagem usuário de erro ou sucesso no login
    * @since: 28/07/2018
    * @version 1.0
    * @author: Lucas Lobato Maciel
    * */

    renderMessage(){
        const {message} = this.state;
        if(!message){
            return null
        }

        return(
            <View>
                <Text>{message}</Text>
            </View>
            )
    };

};


// secureTextEntry  componente do react que esconde o texto digitado como senha
// é uilizada assim para evitar esssa redundância secureTextEntry={true}

const styles = StyleSheet.create({

    conteiner:{
      paddingLeft:10,
        paddingRight:10,

    },
    formInput: {
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5,

    },


});

export default connect(null,{tryLogin})(LoginPage);
