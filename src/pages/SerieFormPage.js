import React from 'react';
import {
    View,
    TextInput,
    StyleSheet,
    Picker,
    Slider,
    Text,
    Button,
    ScrollView,
    KeyboardAvoidingView,
    ActivityIndicator,
    Alert
} from 'react-native';
import {connect} from 'react-redux';
import {FormRow} from '../components';
import {setField, createSerie,setWholeSerie,resetForm,updateSerie} from '../actions';

const GENDER =
    [
        {id:'Policial', nome:'Policial'},
        {id:'Terror', nome:'Terror'},
        {id:'Comédia', nome:'Comédia'},
        {id:'Romance', nome:'Romance'},

    ];

class SerieFormPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
        }
    };

    componentDidMount() {
        const {navigation, setWholeSerie, resetForm} = this.props;
        const {params} = navigation.state;
        if (params && params.serieEdit) {
            return setWholeSerie(params.serieEdit)
        }
        return resetForm();
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" enabled keyboardVerticalOffset={200}>
                <ScrollView>
                    <FormRow first>
                        <TextInput
                            style={styles.formInput}
                            placeholder="Título"
                            value={this.props.serieForm.title}
                            onChangeText={value => this.props.setField('title', value)}
                        />
                    </FormRow>
                    <FormRow>
                        <TextInput
                            style={styles.formInput}
                            placeholder="Url Imagem"
                            value={this.props.serieForm.img}
                            onChangeText={value => this.props.setField('img', value)}
                        />
                    </FormRow>
                    <FormRow>
                        <Picker
                            selectedValue={this.props.serieForm.gender}
                            onValueChange={(itemValue) => this.props.setField('gender', itemValue)}
                        >
                            {
                                GENDER.map((resp, index) => (
                                    <Picker.Item label={resp.nome} value={resp.id} Key={index}/>)
                                )
                            }

                        </Picker>
                    </FormRow>
                    <FormRow>
                        <View style={styles.sameRow}>
                            <Text>Nota: </Text>
                            <Text>{this.props.serieForm.rate}</Text>
                        </View>

                        <Slider
                            value={this.props.serieForm.rate}
                            onValueChange={(value) => this.props.setField('rate', value)}
                            maximumValue={100}
                            step={5}
                            thumbTintColor='blue'
                        />
                    </FormRow>
                    <FormRow>
                        <TextInput
                            style={styles.formInput}
                            placeholder="Descrição"
                            value={this.props.serieForm.description}
                            onChangeText={value => this.props.setField('description', value)}
                            numberOfLines={5}
                            multiline={true}
                        />
                    </FormRow>
                    {this.renderButon()}
                </ScrollView>
            </KeyboardAvoidingView>

        );
    }

    renderButon() {
        const {serieForm} = this.props;
        const {id} = serieForm;
        return (
            this.state.isLoading ?
                <ActivityIndicator size="large" color="#0000ff"/>
                :
                <Button
                    title={id ? 'Editar' : 'Salvar'}
                    onPress={async () => {
                        this.setState({isLoading: true});
                        try {
                            id ?
                                await this.props.updateSerie(serieForm)
                                :
                                await this.props.createSerie(serieForm);
                                this.props.navigation.replace('main');
                        } catch (error) {
                            Alert.alert('Erro', error.message)

                        } finally {
                            this.setState({isLoading: false});
                        }
                    }}/>
        );
    };
}


const styles = StyleSheet.create({

    formInput: {
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5,

    },
    sameRow:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingRight:10,
        paddingLeft:10,
        paddingBottom:10,
    }

});

 function mapStateToPrpos (state) {

     return {

         serieForm: state.serieForm
     }

 }

const mapDispatchToProps = {

    setField,
    createSerie,
    setWholeSerie,
    resetForm,
    updateSerie
};


export default connect (mapStateToPrpos,mapDispatchToProps)(SerieFormPage)
