import React from 'react';
import {View, FlatList, ActivityIndicator, Text, ScrollView, Image} from 'react-native';
import axios from "axios";
import SeriesCardTDM from '../components/SerieCardTDM'

isEven = index => (index % 2 === 0);

export default class SeriesTMD extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listaFilmes: [
                {
                    name: "",
                    overview: '',
                    poster_path: ""
                }
            ]
        }
    }

    componentDidMount() {
        const URL = 'https://api.themoviedb.org/3/discover/tv?api_key=bc3c43e552746273af6dd615b8ab5394&language=pt-BR&sort_by=popularity.desc&page=100';
        axios.get(URL)
            .then(response => {
                this.setState({listaFilmes: response.data.results});
            })
            .catch(error => {
                console.log(error.message)
            });
    }

    render() {
        return (
            <View>
                <ScrollView>
                    {this.state.listaFilmes.map(iten => (
                        <SeriesCardTDM key={iten.name} itens={iten}/>
                    ))}
                </ScrollView>
            </View>

        )
    }


};
