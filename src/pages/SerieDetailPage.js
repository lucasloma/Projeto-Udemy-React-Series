import React from 'react';
import { ScrollView,View,StyleSheet,Image,Button,Alert} from 'react-native';
import LineDetail from '../components/Line';
import LongText from '../components/LongText';
import { connect } from 'react-redux';
import { deleteSerie } from '../actions'

const IMAGEDEFAULT = require('../../resource/icon/picture(64).png');


class SerieDetailPage extends React.Component {

    render(){
        //console.log(this.props.navigation);///pode ser ver todos os dados enviados pelo navegation
     const {navigation}= this.props;
      const {serie} = navigation.state.params;
        return (
        <ScrollView>
            <View style={styles.containerImg}>
                <Image
                    style={styles.imagem}
                    source = {serie.img ? {uri:serie.img} : IMAGEDEFAULT}/>
            </View>
            <LineDetail label = "Título"  component = {serie.title} />
            <LineDetail label = "Gênero"  component = {serie.gender} />
            <LineDetail label = "Nota"  component = {serie.rate} />
            <LongText label = "Descrição"  component = {serie.description} />
           <View style={styles.botoes}>
            <Button
            title='Editar'
            onPress={()=>(navigation.navigate('serieForm',{serieEdit:serie}))}/>
               <Button
                   color='red'
                   title='Delete'
                   onPress={()=>(this.deletarSerie(serie))}/>
           </View>
        </ScrollView>
        );
    }


    async deletarSerie(serie){
        let hasDelete = await this.props.deleteSerie(serie);
        if(hasDelete){
            this.props.navigation.goBack();
        }
    }

}

const styles = StyleSheet.create({

    'imagem': {
        aspectRatio: 1,
        borderWidth: 1,
        borderColor: 'black',
    },
    'containerImg': {
        padding: 10,

    },
    'botoes':{
       // flex:1,
        flexDirection:'row',
        padding: 20,
        margin:20,
        justifyContent:'space-between'
    }
});

const mapStateToProps = null;

const mapDispatchToProps= {
    deleteSerie
};


export default connect(mapStateToProps,mapDispatchToProps)(SerieDetailPage);
