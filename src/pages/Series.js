import React from 'react';
import {View,FlatList, ActivityIndicator} from 'react-native';
import { SeriesCard }from '../components'
import AddSerieCard from '../components/AddSerieCard';
import{ connect } from 'react-redux'
import axios from 'axios'
import { watchSeries }  from '../actions'

//// função que verifica se é par caso for será a primeira coluna.
isEven = index => (index % 2 === 0);

class SeriesPage extends React.Component {

    /*
    * Está função e executada após a criação do render então ela set as séries do firebase
    *
    * */
    componentDidMount() {
        this.props.watchSeries();
    }

    render() {
        const {series, navigation} = this.props;
        if (series === null) {
            return <ActivityIndicator size="large" color="#0000ff"/>
        }
        return (
            <View>
                <FlatList
                    data={[...series, {isLast: true}]} ///operador isLat para saber se  é a ultima serie catalogada
                    renderItem={({item, index}) => (
                        item.isLast ?
                            <AddSerieCard isFirstColunm={isEven(index)}
                                          onNavigate={() => navigation.navigate('serieForm')}/>
                            :
                            <SeriesCard series={item}
                                        isFirstColunm={isEven(index)}
                                        onNavigate={() => navigation.navigate('serieDetail', {serie: item})}/>
                    )}
                    KeyExtractor={item => item.id.toString()}// não resolve problema key
                    numColumns={2}
                    ///ListHeaderComponent={props=>(<View style={style.oi}/>)} componete que pode ser utilizado para um header
                    // e pode passar um estilo caso queira também existe o footer.
                />
            </View>
        )
    }

}

const mapStateToProps =  state =>{
    ///essa série que está sendo enviado para o props do serieRedux
    const {series} = state;
    ///if para avaliar se o state inicial for igual null ele vai ficar em londing
    if(series === null){
        return {series};
    }
    /// passsando o objeto recebido para um array e colocando um ID
    const Keys = Object.keys(series);
    const seriesWhithKey = Keys.map(id => {
        return {...series[id],id}
    });
    return {series:seriesWhithKey};
};

const mapDispatchToProps = {
    watchSeries
};


export default connect (mapStateToProps,mapDispatchToProps)(SeriesPage);
