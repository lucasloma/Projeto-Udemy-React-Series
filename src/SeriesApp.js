import React from 'react';
import Router from './Router';

import {Provider } from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
/// deixa executar uma função dentro de outra função.
import reduxThunk from 'redux-thunk';

import rootReducer from './reducers';
/// faz a composição para poder passar mas de um middleWare
import {composeWithDevTools} from 'remote-redux-devtools';

const store = createStore(rootReducer,composeWithDevTools(
    applyMiddleware(reduxThunk)
));

const SeriesApp = prop => (
    <Provider store={store}>
    <Router/>
    </Provider>
);

export default SeriesApp;