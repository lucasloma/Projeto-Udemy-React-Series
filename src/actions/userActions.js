
import firebase from "firebase";
import {Alert} from "react-native";

export const USER_LOGIN_SUCCESS ='USER_LOGIN_SUCCESS';
const userLogin = user => ({
    type: 'USER_LOGIN_SUCCESS',
    user
});


export const USER_LOGOUT = 'USER_LOGOUT';
const userLogout = user => ({
    type: 'USER_LOGOUT'
});


 export const tryLogin = ({email,password}) => (dispatch,getState) => {
    return firebase.auth().signInWithEmailAndPassword(email, password)
        .then(  user => {
            const action = userLogin(user);
            dispatch(action);
                return user;
            }

        ).catch(error => {
                /*Verifica se usuário está cadastrado se não será possível se cadastratar*/
                if (error.code === 'auth/user-not-found') {

                    return new Promise((resolve, reject) => {

                        /*Função alert alert(titulo, menssagem, [botões],configuração)*/
                        Alert.alert('Ops ! =( ', 'Você ainda não está cadastrado, ' +
                            'deseja se cadastrar com esses dados?',
                            [{
                                text: 'Não',
                                onPress: () => resolve(),
                                style: 'cancel'// para ios
                            },
                                {
                                    text: 'Sim',
                                    onPress: () => {
                                        firebase.auth()
                                            .createUserWithEmailAndPassword(email, password)
                                            .then(resolve)
                                            .catch(reject);
                                    }
                                }
                            ],
                            {cancelable: false} /*opção para usuário não clicar fora do  alerta e pode sair*/
                        );
                    });
                }

                return Promise.reject(error)
            }
        )
};