import firebase from "firebase";
import {Alert} from "react-native"


export const SET_SERIES = 'SET_SERIES';
const setSeries = (series) =>({
    type: SET_SERIES,
    series

});

/*
* Função para retorna as séries do banco de dados
* */

export const watchSeries = () => {
    const {currentUser} = firebase.auth();
    return dispatch => {
        return firebase.database().
        ref(`users/${currentUser.uid}/series`).
        on('value', snapshot => {
            const series = snapshot.val();
            const action = setSeries(series);
            dispatch(action);
        });
    };
};


/*
* Função para deletar a série do banco de dados
* @param : object serie
*
* */

export const deleteSerie = serie => {
    return dispatch => {
        return new Promise((resolve, reject) => {
            Alert.alert('Deletar', `Deseja mesmo deletar a série ${serie.title}`,
                [
                {
                    text: 'Não',
                    onPress: () => {
                        resolve(false);
                    },
                    style: 'cancel' // IOS
                },
                {
                    text: 'Sim',
                    onPress: async () => {
                        const {currentUser} = firebase.auth();
                        try {
                            await firebase.database()
                                .ref(`users/${currentUser.uid}/series/${serie.id}`)
                                .remove();
                            resolve(true);

                        } catch (e) {
                            reject(e);
                        }
                        resolve(true);
                    }
                }
            ],
                {cancelable: false} /*opção para usuário não clicar fora do  alerta e pode sair*/
            )

        });
    }
};
