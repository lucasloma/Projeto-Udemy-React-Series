import firebase from 'firebase'

export const SET_FIELD ='SET_FIELD';
export const setField = (field,value) => ({
    type: 'SET_FIELD',
    field,
    value,
});



////para preencher os dados no formulário de uma série já cadastrada
export const SET_WHOLE_SERIE = 'SET_WHOLE_SERIE';
export const setWholeSerie = (serie)=>({
    type:'SET_WHOLE_SERIE',
    serie
});

export const SERIE_SAVED_SUCCESS = 'SERIE_SAVED_SUCCESS';
const serieSavedSuccess = () =>({
    type: 'SERIE_SAVED_SUCCESS',
});

export const RESET_FORM = 'RESET_FORM';
export const resetForm= () =>({
    type: 'RESET_FORM',
});


export const updateSerie = serie =>{
    const {currentUser} = firebase.auth();
    const db = firebase.database();
    return async dispatch => {
        await db.
        ref(`users/${currentUser.uid}/series/${serie.id}`).
        set(serie);
        dispatch(serieSavedSuccess())
    };

};


export const createSerie = serie =>{
    const {currentUser} = firebase.auth();
    const db = firebase.database();
    ///  substitui a função abaixo await faz que a função espera termina para executar outra coisa
    return async dispatch => {
        await db.
        ref(`users/${currentUser.uid}/series`).
        push(serie);

        dispatch(serieSavedSuccess())
    };

    /// declaração de uma função assincrona que retorna a conexão do banco de dados e salva.
    // async function creatSave (dispatch) {
    //    return await firebase.database().ref(`users/${currentUser.uid}/series`).push(serie);
    // }

};
