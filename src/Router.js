import React from 'react';
import {createStackNavigator } from 'react-navigation';

import LoginPage from "./pages/LoginPage";
import SeriesPage from "./pages/Series"; /// quando for constante não está entrando
import SerieDetailPage from "./pages/SerieDetailPage"; /// quando for constante não está entrando
import SerieFormPage from "./pages/SerieFormPage";
import SeriesTDM from "./pages/SeriesTMD";


const App = createStackNavigator ({
        'login': {
            screen: LoginPage,
            navigationOptions:  {
                title: 'Bem Vindo!!',
            },
        },
        'main': {
            screen: SeriesPage,
            navigationOptions:({navigation})=>{
            }
        },
        'serieForm': {
            screen: SerieFormPage,
            navigationOptions: ({navigation}) => {
                if (navigation.state.params && navigation.state.params.serieEdit) {
                    return {
                        title: navigation.state.params.serieEdit.title
                    }
                }
                return {
                    title: 'Cadastrar',
                }
            },
        },
        'serieDetail': {
            screen: SerieDetailPage,
            navigationOptions: ({navigation}) => {
                ///função para acessar os dados enviados pelo router
                ///console.log(navigation.state.params.serie);
                const {serie} = navigation.state.params;
                return {
                    title: serie.title,
                }
            },
        },
        'seriesTDM': {
            screen: SeriesTDM,
            navigationOptions: {
                title: 'Series TDM!!',
            },
        },
    },
    {
        navigationOptions: {
            title: 'Minhas Séries',
            headerTintColor: 'white',
            headerStyle: {
                backgroundColor: '#3daced',
                borderBottomWidth: 1,
                borderBottomColor: '#c6cece',

            },

            headerTitleStyle: {
                color: 'white',
                fontSize: 30,
                textAlign: "center", /*texto no meio*/
                flexGrow: 1,
            },
        },
    },
);

export default App;
