import React from 'react';
import {View,Text,StyleSheet,Dimensions,Image,TouchableOpacity} from 'react-native';

const AddSeriesCard = ({series,isFirstColunm,onNavigate})=>(

    <TouchableOpacity
        onPress={onNavigate}
        style={[styles.container ,isFirstColunm? styles.firstColunm:styles.lastColunm]}>
        <View style={styles.card}>
            <Image
            source = {require('../../resource/icon/plus.png')}
            style={styles.imgIcon}
            />
        </View>
    </TouchableOpacity>

);

const styles = StyleSheet.create({
    container:{
        // flex: 1,
        width: '50%', //caso eu nao queira um quadro maior quando for um numero impar a divisao dos quadros
        padding: 5,
        // cuida do redimesionamento para cada tipo de tela (no caso altura são duas colunas vindas do flatList).
        height: Dimensions.get('window').width / 2
    },
    card:{
        flex:1,
    },
    firstColunm:{
        paddingLeft:10
    },
    lastColunm:{
        paddingRight:10
    },
    imgIcon:{
        width:'90%',
        height:'90%',
    }
});

export default AddSeriesCard;