import React from 'react';
import {View, Text, StyleSheet,TouchableWithoutFeedback,LayoutAnimation,NativeModules} from 'react-native';

//para animação poder rodar no android e caso de erro na animação não para aplicação.
NativeModules.UIManager.setLayoutAnimationEnabledExperimental && NativeModules.UIManager.setLayoutAnimationEnabledExperimental(true);

export default class LongText extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            isExpanded: false
        }
    }

    /*
    * Função para alterar o estado do isExpand ao ser chamada
    * @size: 24/09/2018
    * @author: Lucas Lobato Maciel
    * */

    toggleIsExpand() {
        const { isExpanded } = this.state;
        this.setState({
            isExpanded : !isExpanded,
        });
    }

    /*
    * Função do react para ser excutada após uma altração do State.
   */
    componentWillUpdate(nextProps, nextState) {
        LayoutAnimation.spring();
        ///console.log(this.state.isExpanded);
    }

    render(){
        const {label = "" ,component = ""} = this.props;
        const {isExpanded} = this.state;
        return(
            <View style={styles.line}>
                <Text style={[styles.cell,styles.cellLabel,]}>{label}</Text>
           <TouchableWithoutFeedback onPress={()=>this.toggleIsExpand()}>

               <View>
                   <Text style={[styles.cell,
                       styles.content,
                       isExpanded ? styles.expand : styles.collapsed
                   ]}>{component}</Text>
               </View>
           </TouchableWithoutFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    line:{
        paddingTop: 3,
        paddingBottom: 30,
    },

    cell:{
        fontSize: 18,
        paddingLeft: 5,
        paddingRight: 5,
    },

    cellLabel:{
        fontWeight: 'bold', /* deixa a font em negrito*/
        flex: 1, /* divida a celula e 4 partes somando a do content*/
        textDecorationLine:'underline',
        paddingBottom:8
    },

    content:{
        flex: 3,
        textAlign:'justify', /// justifica texto para IOS
    },
    collapsed:{
        maxHeight:40
    },
    expand:{
        flex: 1,
    }
});