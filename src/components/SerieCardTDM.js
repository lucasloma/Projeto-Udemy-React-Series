import React from 'react';
import {View,Text,StyleSheet,Dimensions,Image,TouchableOpacity,Alert} from 'react-native';
const IMAGEDEFAULT = require('../../resource/icon/picture(64).png');
const URL = 'https://image.tmdb.org/t/p/w500/';

const SeriesCardTDM = ({itens,onNavigate}) => {
        return (
            <View>
                <TouchableOpacity
                    onPress={onNavigate}
                    style={styles.container}>
                    <View style={styles.card}>
                        <Image
                            source={itens.backdrop_path ? {uri: URL + itens.backdrop_path} : IMAGEDEFAULT}
                            aspectRatio={1}
                            resizeMode="cover"
                        />
                        <View style={styles.cardTitleWrapper}>
                            <Text style={styles.cardTitle}>
                                {itens.name}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.containerInformacao}>
                        <Text style={styles.cellLabel}>Titulo Original:</Text>
                        <Text style={styles.cell}>{itens.original_name}</Text>
                        <Text style={styles.cellLabel}>Origem:</Text>
                        <Text style={styles.cell}>{itens.origin_country}</Text>
                    </View>
                </TouchableOpacity>
            </View>


        );
};


const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-between',
        padding: 5,
        // cuida do redimesionamento para cada tipo de tela (no caso altura são duas colunas vindas do flatList).
    },
    card:{
        marginRight:10,
        flex:5,
        borderWidth:1,
    },
    containerInformacao:{
        flex:6,
        paddingTop: 3,
        paddingBottom: 3,
    },
    cardTitleWrapper:{
        backgroundColor:'#1E90FF',
        height: 40,
        position:'absolute',
        bottom: 0,
        opacity: .8,
        width: '100%',
        paddingTop: 5,
        paddingBottom:5,
        paddingLeft:3,
        paddingRight:3,
        alignItems:'center'
    },
    cardTitle:{
        color:'white',
        fontWeight:'bold',
        fontSize: 13,
    },
    cell:{
        fontSize: 18,
        paddingLeft: 5,
        backgroundColor:'white'
    },

    cellLabel:{
        fontWeight: 'bold', /* deixa a font em negrito*/

    },
});

export default SeriesCardTDM;
