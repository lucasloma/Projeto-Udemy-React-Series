import React from 'react';
import {View,Text,StyleSheet,Dimensions,Image,TouchableOpacity} from 'react-native';
const IMAGEDEFAULT = require('../../resource/icon/picture(64).png');

const SeriesCard = ({series,isFirstColunm,onNavigate})=>(
    <TouchableOpacity
        onPress={onNavigate}
        style={[styles.container ,isFirstColunm? styles.firstColunm:styles.lastColunm]}>
        <View style={styles.card}>
            <Image
            source = {series.img ? {uri:series.img} : IMAGEDEFAULT}
            aspectRatio = {1}
            resizeMode = "cover"
            />
            <View style={styles.cardTitleWrapper}>
            <Text style={styles.cardTitle}>
                {series.title}
            </Text>
            </View>
        </View>
    </TouchableOpacity>

);

const styles = StyleSheet.create({
    container:{
        // flex: 1,
        width: '50%', //caso eu nao queira um quadro maior quando for um numero impar a divisao dos quadros
        padding: 5,
        // cuida do redimesionamento para cada tipo de tela (no caso altura são duas colunas vindas do flatList).
        height: Dimensions.get('window').width / 2
    },
    card:{
        flex:1,
        borderWidth:1,
    },
    cardTitleWrapper:{
        backgroundColor:'#1E90FF',
        height: 40,
        position:'absolute',
        bottom: 0,
        opacity: .8,
        width: '100%',
        paddingTop: 5,
        paddingBottom:5,
        paddingLeft:3,
        paddingRight:3,
        alignItems:'center'
    },
    cardTitle:{
        color:'white',
        fontWeight:'bold',
        fontSize: 13,
    },
    firstColunm:{
        paddingLeft:10
    },
    lastColunm:{
        paddingRight:10

    }
});

export default SeriesCard;
