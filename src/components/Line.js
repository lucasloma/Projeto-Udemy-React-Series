import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const LineDetail =  ({label = "", component = ""}) =>{

    return(

        <View style={styles.line}>
            <Text style={[
                styles.cell,
                styles.cellLabel,
                label.length > 8 ? styles.longLabell: null ]
            }>{label}</Text>
            <Text style={[styles.cell, styles.content]}>{component}</Text>
        </View>

    );

};


export default LineDetail;

const styles = StyleSheet.create({

    line:{
        flexDirection: 'row', /* como se fosse uma linha da tabela*/
        paddingTop: 3,
        paddingBottom: 3,
        borderWidth: 1,
        borderColor: '#e3e4e5'

    },

    cell:{
        fontSize: 18,
        paddingLeft: 5,
    },

    cellLabel:{
        fontWeight: 'bold', /* deixa a font em negrito*/
        flex: 1, /* divida a celula e 4 partes somando a do content*/

    },

    content:{
        flex: 3,

    },

    longLabell:{
        fontSize: 13,
    }

});
