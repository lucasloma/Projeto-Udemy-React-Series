import { SET_SERIES } from '../../src/actions'


export default function serieReducers(state = null, action) {
   switch (action.type){
       case SET_SERIES:
           return action.series;
       default:
           return state
   }

};
