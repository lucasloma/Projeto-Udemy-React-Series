import {SET_FIELD,SERIE_SAVED_SUCCESS,SET_WHOLE_SERIE,RESET_FORM} from '../actions'

const INITIAL_STATE = {

    title: '',
    gender: '1',
    rate: 0,
    img: '',
    description: '',
    id:null
};


export default function serieFormReducers(state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_FIELD:
            const newState = {...state}; /// estou colonando o state
            newState[action.field] = action.value;
            return newState;
        case SET_WHOLE_SERIE:
            return action.serie;
        case RESET_FORM:
            return INITIAL_STATE;
        case SERIE_SAVED_SUCCESS:
            return INITIAL_STATE;
        default:
            return state;
    }
};
