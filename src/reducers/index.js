import {combineReducers} from 'redux';

import userReducers from './userReducers'
import serieFormReducers from './SerieFormReducers'
import serieReducers from './serieReducers'

export default combineReducers({
    user: userReducers,
    serieForm: serieFormReducers,
    series: serieReducers,
});
